#!/bin/bash

if [ "$(whoami)" == "root" ]; then
    exit 1
fi



echo "Instalar y configurar BSPWM"
echo "partiendo de una instalación mínima de"
echo "Debian GNU/Linux 12 - Bookworm - 64 bits"
echo "También se instalan varias apps, flatpak,"
echo "kdenlive, virt-manager, libreoffice"

sudo apt update

# instalar servidor gráfico y bspwm
sudo apt install -y xorg xterm lightdm lightdm-gtk-greeter bspwm sxhkd polybar rofi jgmenu suckless-tools tint2 feh nitrogen terminator picom conky dunst nm-tray nm-tray-l10n xtitle xfce4-terminal thunar arandr gdebi curl htop git wget inxi vim build-essential devscripts neofetch glances


# descargar repositorio gitlab
cd /tmp/
git clone https://gitlab.com/linux-en-casa/bspwm.git
cd /tmp/bspwm/
git branch -a
git checkout -b bspwm-color origin/bspwm-color 
git pull origin bspwm-color 
mkdir -p ~/.config
cp -r * ~/.config/

# descargar fuentes de nerdfonts
cd /tmp
git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git
/tmp/nerd-fonts/install.sh 


# instalar mas paquetes
sudo apt install -y firefox-esr gimp audacity isenkram-cli clementine wireplumber pipewire-media-session- obs-studio libreoffice transmission libspa-0.2-bluetooth vlc guvcview blender gedit volumeicon-alsa flameshot ristretto calibre inkscape blender mpv xfce4-screenshooter
sudo apt install -y mediainfo curl sox inxi unrar simplescreenrecorder screenkey gparted sayonara synaptic policykit-1-gnome


# crear directorios de usuarios
sudo apt install -y xdg-user-dirs
xdg-user-dirs-update 

# paquetes adicionales
sudo apt install -y libreoffice-gtk3 libreoffice-l10n-es 

# para algunas apps como synaptic
sudo apt install -y software-properties-common software-properties-gtk

# pipewire
sudo apt install -y wireplumber pipewire-media-session- pipewire-pulse pipewire-alsa libspa-0.2-bluetooth 


# Mensaje de Instalado

echo "BSPWM Instalado"

# instalar flatpak
echo "Instalar flatpak flathub y algunos paquetes"
sleep 5

sudo apt install flatpak

sudo flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

flatpak install flathub org.kde.kdenlive



#  instalar  virt-manager
sudo apt install qemu-system libvirt-daemon-system virt-manager
sudo adduser $USER libvirt

echo "..."
echo "..."
echo "fin de instalación"

