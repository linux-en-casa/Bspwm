#!/bin/bash

if [ "$(whoami)" == "root" ]; then
    exit 1
fi

echo "Instalar y configurar BSPWM"
echo "En Fedora Linux"
echo "Primeras pruebas - errores por todos lados:"
echo "no se instalan las nerdfonts, no se crean los espacios de trabajo"
echo "ADVERTENCIA, no usar :-( sleep 20 -- cancelar antes de empezar"
sleep 20
sudo dnf update

# instalar y configurar bspwm - sxhkd - rofi - polybar
sudo dnf install bspwm sxhkd polybar rofi jgmenu feh nitrogen terminator picom conky dunst  xfce4-terminal thunar arandr curl htop git wget inxi vim neofetch wlogout btop
# para el fondo de pantalla uso feh pero también instalo nitrogen que es más sencillo

# descargar repositorio gitlab
cd /tmp/
git clone https://gitlab.com/linux-en-casa/bspwm.git
cd /tmp/bspwm/
git branch -a
git checkout -b bspwm-color origin/bspwm-color 
git pull origin bspwm-color 
mkdir -p ~/.config
cp -r * ~/.config/

# descargar fuentes de nerdfonts
echo "El tema utiliza las fuentes nerd-fonts"
echo "Se descargan de github"
sleep 2
cd /tmp
git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git
/tmp/nerd-fonts/install.sh 



# Mensaje de Instalado

echo "BSPWM Instalado"
sleep 3
echo "..."
echo "..."
echo "Ajustar la resolución de la pantalla"
echo "utilizar arandr y crear un script con el sig. nombre,"
echo "resolucion.sh   ---   para una sola pantalla"
echo "resolucion-2p.sh   ---   para 2 pantallas"
echo "la configuracion de las barras de polybar cambia"
echo "dependiendo el numero de pantallas que se detecten"
echo "solo lo hice para 1 ó 2 pantallas"
echo "tal vez lo ajuste para 3 pantallas en un futuro"
echo "..."
echo "..."
sleep 3
echo "fin de instalación"

