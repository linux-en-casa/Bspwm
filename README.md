# Bspwm

Este es mi primer intento en GitLab.  Estoy configurando bspwm utilizando Git.

El proyecto bspwm tiene ahora 5 ramas o branches:
1. branch main - Tiene este readme que estoy modificando a toda hora y los temas de rofi que deben ir en el directorio
.local/share/rofi/themes

2.  branch master - es el branch con la configuración básica de bspwm - sin colores ni nada.
inicialmente se llamaba polybar-old pero al subirlo a gitlab debí dar mal algún comando y se cambió el nombre

3.  branch polybar-new  -  es el branch de bspwm con la configuración colorida que tomé de axyl linux

Los branches 2 y 3 tienen demasiados errores o detalles y utilizan nitrogen para el fondo de pantalla.  Les hice algunos cambios y ahora tengo 2 nuevas ramas.

4.  Un 4to branch llamado bspwm-feh en el que quito nitrogen y ahora uso feh para el fondo de la pantalla.  Sería el reemplazo del branch numero 2.  Además modifico algunos scripts para que la detección de los monitores sea automática. Y puedo insertar y/o quitar el monitor externo y los Espacios de trabajo se ajustan automáticamente.

5.  El 5to branch se llama bspwm-color.  Y es una mejora del branch número 3.  También usa feh y tiene todas las mejoras del branch 4, es decir puedo quitar o poner el monitor externo y los Espacios de trabajo se ajustan automáticamente.   Todos menos el espacio de trabajo 0 que no logro pasar del monitor externo al monitor interno. 


## Notas:
Estoy probando a descargar las ramas y probarlos en una VM.

Están todos los commits de todas las ramas - demasiados commits - pues a cada cambio que hacía creaba un nuevo commit.

Los branches 2 y 3 solo tienen los archivos o directorios que van en ~/.config 
Los archivos de los temas que necesita rofi están en el branch main
Y faltarían los scripts de la configuración de las pantallas que se crean con arandr

Estaba haciendo unas pruebas con EWW pero ahora no encuentro ese branch.  Debí haber dado mal algún comando y se borró
o se mezcló con el branch que ahora se llama master  -  tal vez por eso hay tantas carpetas en los branches 2 y 3 que no necesito (pero no quiero borrar nada :-).



## TL/DR 

Hice un primer video de prueba para los branches 2 y 3; que muestro en este enlace
https://youtu.be/pjr0vPEE7a0

Para los branches 4 y 5 hice este otro video:
https://youtube.com/live/gGecSokVsNI?feature=share

Y un tercer video configurando en directo los branches 4 y 5 con git en Debian 12
https://youtube.com/live/9FqJOmprmAg?feature=share


# Como descargar las 3 ramas o "branches"?

Creo un primer directorio, por ejemplo lo llamo bspwm-gitlab

$ mkdir bspwm-gitlab

$ cd bspwm-gitlab

Hago un git clone, con:

$ git clone https://gitlab.com/linux-en-casa/bspwm.git 

Esto crea o descarga un directorio llamado bspwm

$ cd bspwm

$ ls 

Al dar ls va a aparecer este README.md y los temas de rofi, pues el comando descarga inicialmente la rama main

Puedo ver las ramas o "branches" que hay dando el comando:

$ git branch -a

y se van a listar las 3 ramas, main, master, polybar-new.  Y el HEAD apunta a main


## Descargar los archivos del Tema Oscuro de polybar

Debo descargar los archivos de la rama master dando 2 comandos,

$ git checkout -b master origin/master 

$ git pull origin master 

Y si doy de nuevo un ls  ahora deben aparecer los archivos de configuración de bspwm del tema "polybar oscuro" o el primer tema que se llamaba "polybar-old"


##  Descargar los archivos del Tema Colorido de polybar

Y, finalmente si quisiera descargar la rama "polybar-new" daría otros 2 comandos:

$ git checkout -b polybar-new origin/polybar-new

$ git pull origin polybar-new 

y si doy un nuevo ls ahora se van a listar los archivos de bspwm que corresponden al tema colorido de polybar que está basado en Axyl Linux 

Y a partir de ahora solo debo cambiar de rama dando


$ git checkout main
$ git checkout master
$ git checkout polybar-new


##  Copiar los archivos.
Los archivos de las ramas master o polybar-new se copiarían en el directorio .config. 
Por ahora lo haría en forma manual,
Es decir copio bien sea:
los archivos de master para tener un polybar oscuro  (carpetas bspwm, conky, dunst, jgmenu, picom, polybar, rofi, sxhkd)

o copio los archivos de polybar-new para tener un polybar colorido ( carpetas, bspwm, conky, dunst, jgmenu, picom, rofi, sxhkd).

 ...tal vez no sean necesarios todos los subdirectorios....  tal vez... 


También debo copiar los temas de rofi que están en la rama main y se copian en
~/.local/share/rofi/themes

Y faltarían los 2 archivos con la resolucón de pantalla que genera arandr
que deben ir en
~/.screenlayout
los archivos se deben nombrar
resolucion.sh
resolucion-2p.sh 

para configurar 1 o 2 pantallas.

Adicional, se deben modificar los archivos de bspwm y de polybar para que los nombres de las pantallas sean los correctos,  dentro de los archivos hay varios comentarios explicando.


## Aplicaciones o paquetes que se deben instalar
Partiendo desde una instalación mínima de Linux Debian:

bspwm y sxhkd  (recordar que los descargo e instalo desde github pues los paquetes de Debian dan un error al abrir algunas ventanas)

conky, para las ventanas de información del sistema que se ven en la pantalla

dunst, para las notificaciones

jgmenu, el menu para lanzar aplicaciones que está en la barra externa de polybar

feh, para el fondo de pantalla

nitrogen, para el fondo de la pantalla

picom, para las transparencias

polybar, barra o panel de las 2 pantallas

rofi, otro lanzador de aplicaciones

dmenu, otro lanzador de aplicaciones

volumeicon-alsa, para que se vea el icono de volumen en el area de notificaciones de polybar 

xfce4-screenshooter, para las capturas de la pantalla

xtitle, lo necesito para mostrar los nombres de las ventanas en un script llamado "ShowHidden.sh" de rofi

policykit-1,  para abrir apps GUI que necesitan password de administrador

nm-tray

nm-tray-l10n

Estos últimos 2 paquetes son para probar el area de notificaciones de polybar 

los emuladores de terminal que uso son terminator y xfce4-terminal

Tambien instalo lxappearance y adwaita-icon-themes.

Y en Debian 12 se necesita el paquete "psmisc" para que funcione el comando killall.

No puede faltar build-essential, htop, git.

También se necesitan las fuentes nerd-font que las usa la barra colorida de polybar.  Las descargo desde github
Creo que descargué casi todo el repo dando

git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git

Y luego daba

./install.sh

Aunque esto descarga todas las fuentes, creo que es 1 GB de archivos


# Hasta aquí va este README

# Todo lo que sigue es del README original.  No lo borro pues Toy aprendiendo, sorry







To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/linux-en-casa/Bspwm.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/linux-en-casa/Bspwm/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
